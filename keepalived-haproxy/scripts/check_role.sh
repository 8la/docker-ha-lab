#!/bin/bash
#
# Tells you if this node is the primary or secondary with keepalived

conf=/etc/keepalived/keepalived.conf


while true; do
    sleep 5;
    # Get the vip from the config
    vip=$(cat $conf|grep virtual_ipaddress -A1|tail -1|sed 's/^ *//g'|sed 's/\([^\/]*\)\/.*/\1/g');
    

    if ip addr | grep -q "$vip";
    then
        state="MASTER"
    else
        state="BACKUP"
    fi
    msg="VRRP role is $state"
    # We had previous state
    if [[ ! -z $prev_state ]];
    then
        if [[ ! $state == $prev_state ]];
        then
            msg="¡ROLE CHANGE!   $prev_state ==> $state"
        fi
    fi
    # Save state for next check
    prev_state=$state
    echo $msg
done