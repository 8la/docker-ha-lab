# Docker HA-Lab: Keepalive

Docker-compose setup to play with keepalive ha cluster formed by 2 nodes and using interface tracking.

* Base image: https://github.com/arc-ts/keepalived
* Keepalive knowledge reference: https://www.redhat.com/sysadmin/advanced-keepalived
## Description
The image is prepared for track `eth1` so here you will find instructions of how to play and whatch what happens.

If you want to see changes realtime, keep docker-compose command attached, if you run detached, you will need to use `docker container logs... ` in order to whatch cluster STDOUT.

## Instructions

```bash
cd keepalived-haproxy && docker-compose up
/mnt/d/git/docker-halab/keepalived-haproxy$ docker-compose up
Creating network "keepalived-haproxy_default" with the default driver
Creating network "keepalived-haproxy_ha" with the default driver
Creating keepalived-haproxy_vrrp-node0_1 ... done
Creating keepalived-haproxy_vrrp-node1_1 ... done
Attaching to keepalived-haproxy_vrrp-node1_1, keepalived-haproxy_vrrp-node0_1
vrrp-node1_1  | Sun Nov  1 11:15:46 2020: Starting Keepalived v2.0.11 (01/08,2019), git commit v3.8.2-16-gb0db67a5a9
vrrp-node1_1  | Sun Nov  1 11:15:46 2020: Running on Linux 4.19.104-microsoft-standard #1 SMP Wed Feb 19 06:37:35 UTC 2020 (built for Linux 4.4.6)
vrrp-node1_1  | Sun Nov  1 11:15:46 2020: Command line: '/usr/sbin/keepalived' '-n' '-l' '-f' '/etc/keepalived/keepalived.conf'
vrrp-node0_1  | Sun Nov  1 11:15:47 2020: Starting Keepalived v2.0.11 (01/08,2019), git commit v3.8.2-16-gb0db67a5a9
vrrp-node0_1  | Sun Nov  1 11:15:47 2020: Running on Linux 4.19.104-microsoft-standard #1 SMP Wed Feb 19 06:37:35 UTC 2020 (built for Linux 4.4.6)
vrrp-node0_1  | Sun Nov  1 11:15:47 2020: Command line: '/usr/sbin/keepalived' '-n' '-l' '-f' '/etc/keepalived/keepalived.conf'
vrrp-node0_1  | Sun Nov  1 11:15:47 2020: Opening file '/etc/keepalived/keepalived.conf'.
vrrp-node1_1  | Sun Nov  1 11:15:46 2020: Opening file '/etc/keepalived/keepalived.conf'.
vrrp-node1_1  | Sun Nov  1 11:15:46 2020: Starting VRRP child process, pid=24
vrrp-node1_1  | Sun Nov  1 11:15:46 2020: Registering Kernel netlink reflector
vrrp-node1_1  | Sun Nov  1 11:15:46 2020: Registering Kernel netlink command channel
vrrp-node0_1  | Sun Nov  1 11:15:47 2020: Starting VRRP child process, pid=24
vrrp-node1_1  | Sun Nov  1 11:15:47 2020: Opening file '/etc/keepalived/keepalived.conf'.
vrrp-node1_1  | Sun Nov  1 11:15:47 2020: Registering gratuitous ARP shared channel
vrrp-node1_1  | Sun Nov  1 11:15:47 2020: (MAIN) Entering BACKUP STATE (init)
vrrp-node0_1  | Sun Nov  1 11:15:47 2020: Registering Kernel netlink reflector
vrrp-node0_1  | Sun Nov  1 11:15:47 2020: Registering Kernel netlink command channel
vrrp-node0_1  | Sun Nov  1 11:15:47 2020: Opening file '/etc/keepalived/keepalived.conf'.
vrrp-node0_1  | Sun Nov  1 11:15:47 2020: Registering gratuitous ARP shared channel
vrrp-node0_1  | Sun Nov  1 11:15:47 2020: (MAIN) Entering BACKUP STATE (init)
vrrp-node1_1  | VRRP role is BACKUP
vrrp-node0_1  | VRRP role is BACKUP
vrrp-node1_1  | VRRP role is BACKUP
vrrp-node0_1  | VRRP role is BACKUP
vrrp-node1_1  | VRRP role is BACKUP
vrrp-node0_1  | VRRP role is BACKUP
vrrp-node1_1  | Sun Nov  1 11:15:50 2020: (MAIN) Entering MASTER STATE
vrrp-node1_1  | ¡ROLE CHANGE! BACKUP ==> MASTER
vrrp-node0_1  | VRRP role is BACKUP
vrrp-node1_1  | VRRP role is MASTER
```

### Checking if keepalive is doing his work

Now we have the cluster up and running, as you can see on the output from the check_role script:
* node1 is **MASTER**
* node0 is **BACKUP**

Now we can play with eth1 (which is the tracked_interface) and watch how the cluster behaves:

We simply execute an down-up on `eth1` interface on the **MASTER** container:
```bash
docker exec  keepalived-haproxy_vrrp-node1_1 ip l set eth1 down && ip l set eth1 up
```

On the docker-compose shell we can se: 

```bash
vrrp-node1_1  | VRRP role is MASTER
vrrp-node1_1  | Sun Nov  1 11:31:09 2020: Netlink reports eth1 down
vrrp-node1_1  | Sun Nov  1 11:31:09 2020: (MAIN) Entering FAULT STATE
vrrp-node1_1  | Sun Nov  1 11:31:09 2020: (MAIN) sent 0 priority
vrrp-node0_1  | Sun Nov  1 11:31:09 2020: (MAIN) Backup received priority 0 advertisement
vrrp-node0_1  | Sun Nov  1 11:31:09 2020: (MAIN) Backup received priority 0 advertisement
vrrp-node0_1  | VRRP role is BACKUP
vrrp-node0_1  | Sun Nov  1 11:31:09 2020: (MAIN) Entering MASTER STATE
vrrp-node1_1  | ¡ROLE CHANGE! MASTER ==> BACKUP
vrrp-node0_1  | ¡ROLE CHANGE! BACKUP ==> MASTER
vrrp-node1_1  | VRRP role is BACKUP
vrrp-node0_1  | VRRP role is MASTER
```

As you can check, VRRP is so sensible to the quick upDown we have done that from now on, roles have changed:
* node1 is **BACKUP**
* node0 is **MASTER**

# TODO:

* Detect FAULT state on check_role script.
* Whe can improve the setup using [track_script|track_process] + service (samba,haproxy..).

# DISCLAIMER

**YES!!** The logs where taken on a Windows DockerDesktop setup :P