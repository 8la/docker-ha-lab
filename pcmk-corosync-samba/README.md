# Docker HA-Lab: Corosync+Pacemaker

This is a Docker-compose setup to play with a little corosync+pacemaker cluster of 2 nodes

# TODO

1.- Search for a decent base image and documentation about docker and corosync+pacemaker.
2.- Implement the docker-compose basic setup.
3.- Customize config.
4.- Improve with some service orchestration.